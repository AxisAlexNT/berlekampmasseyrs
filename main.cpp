#include <iostream>
#include <cstdio>
#include <vector>
#include <set>
#include <cstring>
#include <random>

using namespace std;

enum EncoderType {
    BCH,
    RS
};

// Make this switch constant global in order to enable compile-time optimizations
static const auto et = BCH;

const int resolution = 10000000;

// Prepare random number generator for bitvector and noise generation
std::random_device rd{};
std::mt19937 gen{rd()};
std::uniform_int_distribution<int> bitgen(0, 1);
std::uniform_int_distribution<int> coin_gen(0, resolution);
std::uniform_real_distribution<float> prob_gen(0.0, 1.0);

// Builds bitmask/degree representation for elements of extended binary galois field GF(2^m)
class ExtendedBinaryFiniteField {
private:
    vector<int> degree_to_bitmask;
    vector<int> bitmask_to_degree;
public:
    // Number of items in this field (including zero):
    const int power;

    ExtendedBinaryFiniteField(
            const int m,
            const int primitive_polynomial_bitmask
    ) : power(1 << m),
        degree_to_bitmask((1 << m), 0),
        bitmask_to_degree((1 << m), 0) {

        // Bitmask after which cyclic shift must be XORed with modulus polynomial:
        const auto highest_one_bitmask = power;
        // Initialize representation for alpha^0 = 1:
        degree_to_bitmask[0] = 1;
        // Cyclically shift of previous bitmask and optionally XOR with modulus in order to get next degree's bitmask:
        for (int deg = 1; deg <= power - 2; ++deg) {
            int next_bitmask = (degree_to_bitmask[deg - 1] << 1);
            if ((next_bitmask & highest_one_bitmask) || (next_bitmask >= highest_one_bitmask)) {
                next_bitmask ^= primitive_polynomial_bitmask;
            }
            degree_to_bitmask[deg] = next_bitmask;
        }

        // Build inverse mapping:
        for (int deg = 0; deg <= power - 2; ++deg) {
            bitmask_to_degree[degree_to_bitmask[deg]] = deg;
        }
        // Zero is not an element of multiplicative group, so initialize it to something huge in order to notice in case of bug:
        bitmask_to_degree[0] = -10000000;
        bitmask_to_degree[1] = 0;
    }

    [[nodiscard]] inline int get_mult_bitmask(const int a_bitmask, const int b_bitmask) const {
        // Since zero is not a member of multiplicative group, process it exclusively:
        if ((a_bitmask == 0) || (b_bitmask == 0)) {
            return 0;
        }
        // Otherwise do multiplication in multiplicative group:
        const int a_deg = bitmask_to_degree[a_bitmask];
        const int b_deg = bitmask_to_degree[b_bitmask];
        const int r_deg = (power - 1 + a_deg + b_deg) % (power - 1);
        const int r_bitmask = degree_to_bitmask[r_deg];
        return r_bitmask;
    }

    [[nodiscard]] inline int get_sum_bitmask(const int a_bitmask, const int b_bitmask) const {
        return a_bitmask ^ b_bitmask;
    }

    [[nodiscard]] inline int get_sum_degree(const int a_deg, const int b_deg) const {
        const int a_bitmask = degree_to_bitmask[a_deg];
        const int b_bitmask = degree_to_bitmask[b_deg];
        const int r_bitmask = a_bitmask ^ b_bitmask;
        return bitmask_to_degree[r_bitmask];
    }

    [[nodiscard]] inline int get_mult_degree(const int a_deg, const int b_deg) const {
        return ((power - 1 + a_deg + b_deg) % (power - 1));
    }

    [[nodiscard]] inline int get_inv_degree(const int a_deg) const {
        return (power - 1 - a_deg) % (power - 1);
    }

    [[nodiscard]] inline int get_inv_bitmask(const int a_bitmask) const {
        const auto a_deg = bitmask_to_degree[a_bitmask];
        const auto inv_deg = get_inv_degree(a_deg);
        return degree_to_bitmask[inv_deg];
    }

    [[nodiscard]] inline int get_bitmask_by_degree(const int a_deg) const {
        return degree_to_bitmask[a_deg];
    }

    [[nodiscard]] inline int get_degree_by_bitmask(const int a_bitmask) const {
        // Assertion for RS-decoder (but there seems to be problem with input test data)
        if (a_bitmask == 0) {
            throw std::logic_error("Zero is not an element of GF's multiplicative group");
        }
        return bitmask_to_degree[a_bitmask];
    }

    // Times is different from mult, because it is induced action of N over elements (add together t times)
    [[nodiscard]] inline int get_times_bitmask(const int a_bitmask, const int t) const {
        // Since our field has Char(K) = 2:
        if (t % 2 == 0) {
            return 0;
        } else {
            return a_bitmask;
        }
    }
};

// Describes polynomials with coefficients from Galois Field.
// Coefficients are stored as bitmasks (from practical POV that seems to be worse than storing them as degrees of primitive, but too late to change that).
class GFPolynomial {
private:
    [[nodiscard]] inline static vector<int> normalize_coeffs(const vector<int> &v) {
        vector<int> result(v);
        normalize(result);
        return result;
    }

public:
    vector<int> bitmask_coeffs;
    int degree;

    inline static void normalize(vector<int> &v) {
        while (!v.empty() && (v.back() == 0)) {
            v.pop_back();
        }
    }


    explicit GFPolynomial(const vector<int> &coeffBitmask) {
        bitmask_coeffs = normalize_coeffs(coeffBitmask);
        if (bitmask_coeffs.empty()) {
            bitmask_coeffs.push_back(0);
        }
        degree = bitmask_coeffs.size() - 1;
    }

    inline int operator[](const int i) const {
        return bitmask_coeffs[i];
    }

    inline static GFPolynomial
    multiply(const GFPolynomial &p, const GFPolynomial &q, const ExtendedBinaryFiniteField &gf) {
        vector<int> result_bitmask;
        const auto p_deg = p.degree, q_deg = q.degree;
        result_bitmask.resize(1 + p_deg + q_deg);

        for (int i = 0; i <= p_deg; ++i) {
            for (int j = 0; j <= q_deg; ++j) {
                const auto p_coeff = p[i];
                const auto q_coeff = q[j];
                const auto new_term = gf.get_mult_bitmask(p_coeff, q_coeff);
                const auto old_coeff = result_bitmask[i + j];
                const auto new_coeff = gf.get_sum_bitmask(old_coeff, new_term);
                result_bitmask[i + j] = new_coeff;
            }
        }

        return GFPolynomial(result_bitmask);
    }

    inline static GFPolynomial sum(const GFPolynomial &p, const GFPolynomial &q, const ExtendedBinaryFiniteField &gf) {
        vector<int> result_bitmask;
        const auto p_deg = p.degree, q_deg = q.degree;
        result_bitmask.resize(1 + max(p_deg, q_deg));

        const auto common_length = min(p_deg, q_deg);

        for (int i = 0; i <= common_length; ++i) {
            result_bitmask[i] = gf.get_sum_bitmask(p.bitmask_coeffs[i], q.bitmask_coeffs[i]);
        }

        const auto &greater_deg_p = (p_deg > q_deg) ? p : q;

        for (int i = 1 + common_length; i <= greater_deg_p.degree; ++i) {
            result_bitmask[i] = greater_deg_p[i];
        }

        return GFPolynomial(result_bitmask);
    }

    // Division with the remainder
    inline static pair<GFPolynomial, GFPolynomial>
    divide(const GFPolynomial &p, const GFPolynomial &q, const ExtendedBinaryFiniteField &gf) {
        vector<int> quotient_coeffs(1 + max(p.degree - q.degree, 0));

        auto p_coeffs(p.bitmask_coeffs);
        const auto &q_coeffs = q.bitmask_coeffs;

        while (true) {
            if (p_coeffs.size() < q_coeffs.size()) {
                return make_pair(GFPolynomial(quotient_coeffs), GFPolynomial(p_coeffs));
            }

            int quot_deg = (int) (p_coeffs.size() - q_coeffs.size());

            quotient_coeffs[quot_deg] =
                    gf.get_bitmask_by_degree(
                            (
                                    (gf.power - 1)
                                    +
                                    gf.get_degree_by_bitmask(p_coeffs.back())
                                    -
                                    gf.get_degree_by_bitmask(q_coeffs.back())
                            ) % (gf.power - 1)
                    );

            for (int i = 0; i < q_coeffs.size(); ++i) {
                p_coeffs[i + quot_deg] = gf.get_sum_bitmask(
                        p_coeffs[i + quot_deg],
                        gf.get_mult_bitmask(
                                quotient_coeffs[quot_deg],
                                q_coeffs[i]
                        )
                );
            }

            normalize(p_coeffs);
        }
    }

    inline static int
    evaluate_as_bitmask(const GFPolynomial &p, const int point_bitmask, const ExtendedBinaryFiniteField &gf) {
        if (p.bitmask_coeffs.empty()) {
            return 0;
        } else if (p.bitmask_coeffs.size() == 1) {
            return p[0];
        }

        const auto point_degree = gf.get_degree_by_bitmask(point_bitmask);

        /*
        // Naive solution seems to be too slow:
        int result_bitmask = 0;
        int x_power_bitmask = 1;
        for (int i = 0; i <= p.degree; ++i) {
            const auto ith_coeff_bitmask = p[i];
            if (ith_coeff_bitmask != 0) {
                const auto old_result_bitmask = result_bitmask;
                const auto new_term_bitmask = gf.get_mult_bitmask(ith_coeff_bitmask, x_power_bitmask);
                const auto new_result_bitmask = gf.get_sum_bitmask(old_result_bitmask, new_term_bitmask);
                result_bitmask = new_result_bitmask;
            }
            x_power_bitmask = gf.get_mult_bitmask(x_power_bitmask, point_bitmask);
        }
        return result_bitmask;
         */

        // Partially unrolled naive method.
        // It is actually faster than Horner's, because it requires fewer transformations bitmask <-> degree.
        // Probably, if I've stored coefficients as degrees of primitive and not as bitmasks, Horner's method would have been faster.
        int result_bitmask = p[0];
        for (int deg = 1; deg <= p.degree; ++deg) {
            const auto coeff_bitmask = p[deg];
            if (coeff_bitmask == 0) {
                continue;
            }
            const auto p_deg_degree = (point_degree * deg) % (gf.power - 1);
            const auto coeff_degree = gf.get_degree_by_bitmask(coeff_bitmask);
            const auto term_degree = gf.get_mult_degree(p_deg_degree, coeff_degree);
            const auto term_bitmask = gf.get_bitmask_by_degree(term_degree);
            result_bitmask = gf.get_sum_bitmask(result_bitmask, term_bitmask);
        }

        return result_bitmask;

        /*
       // Horner's method:
       int acc_bitmask = p[p.degree];
       for (int i = p.degree-1; i >= 0; --i){
           acc_bitmask = gf.get_mult_bitmask(acc_bitmask, point_bitmask);
           acc_bitmask = gf.get_sum_bitmask(acc_bitmask, p[i]);
       }
       return acc_bitmask;
         */
    }

    // Formal derivative:
    inline static GFPolynomial
    derivate(const GFPolynomial &p, const ExtendedBinaryFiniteField &gf) {
        vector<int> result_coeffs(p.degree);
        for (int i = 1; i <= p.degree; ++i) {
            result_coeffs[i - 1] = gf.get_times_bitmask(p[i], i);
        }
        return GFPolynomial(result_coeffs);
    }
};

// Logarithm for detection of Galois Field extension factor from (1+n):
inline static int log2(int z) {
    if (z < 1) {
        throw std::logic_error("Attempt to compute integral log2 for z < 1?");
    }

    int answer = -1;
    while (z > 0) {
        ++answer;
        z >>= 1;
    }

    return answer;
}


class CCEncoder {
private:
    const int n, m, constr_min_dist;
    int k = -1;
    const ExtendedBinaryFiniteField &gf;
    const int b;
    GFPolynomial generator;
public:

    CCEncoder(const int n,
              const int m,
              const int constrMinDist,
              const int b,
              const ExtendedBinaryFiniteField &gf) :
            n(n),
            m(m),
            generator(GFPolynomial({1})),
            constr_min_dist(constrMinDist),
            b(b),
            gf(gf) {}

    void init() {
        vector<int> required_roots_deg;
        // Generator polynomial must have these sequential degrees as roots:
        for (int i = b; i <= b + constr_min_dist - 2; ++i) {
            required_roots_deg.push_back(i);
        }

        set<int> degrees_of_roots_of_generating_polynomial;

        if (et == RS) {
            // In case of RS there are no other roots
            degrees_of_roots_of_generating_polynomial =
                    set<int>(required_roots_deg.cbegin(),
                             required_roots_deg.cend());
        } else if (et == BCH) {
            // In a general case of BCH codes, we need to add all elements of root candidate's cyclotomic class as roots:
            vector<set<int>> cyclotomic_classes(required_roots_deg.size());

            for (int i = 0; i < required_roots_deg.size(); ++i) {
                const auto root_deg = required_roots_deg[i];
                auto current_deg = root_deg;
                while (true) {
                    if (cyclotomic_classes[i].find(current_deg) != cyclotomic_classes[i].end()) {
                        break;
                    }
                    cyclotomic_classes[i].insert(current_deg);
                    current_deg *= 2; // Since p === 2, GF(2^m)
                    current_deg %= n;
                }
            }

            for (const auto &cc: cyclotomic_classes) {
                for (const auto deg: cc) {
                    degrees_of_roots_of_generating_polynomial.insert(deg);
                }
            }
        } else {
            throw std::runtime_error("Unknown encoder type");
        }

        // And then multiply corresponding monomes to gain generating polynomial:
        for (const auto root_degree: degrees_of_roots_of_generating_polynomial) {
            const auto monome = GFPolynomial({gf.get_bitmask_by_degree(root_degree), 1});
            generator = GFPolynomial::multiply(generator, monome, gf);
        }
        k = n - generator.degree;
    }

    [[nodiscard]] int get_dimension() const {
        return k;
    }

    [[nodiscard]] GFPolynomial get_generator() const {
        return generator;
    }

    [[nodiscard]] vector<int> systematic_encode(const vector<int> &inf) const {
        if (inf.size() != k) {
            throw std::logic_error("Information vector is not of a code dimension length??");
        }

        // Systematic encoding is done as c(z) = inf(z) * z^(n-k) + m(z) where m(z) is inf(z)z^(n-k) mod g(z) and g(z) is generator polynomial:

        vector<int> xnk_coeffs(n - k + 1, 0);
        xnk_coeffs[n - k] = 1;
        const auto xnk = GFPolynomial(xnk_coeffs);

        vector<int> normalized_inf(inf);
        GFPolynomial::normalize(normalized_inf);

        const auto shifted_inf = GFPolynomial::multiply(GFPolynomial(normalized_inf), xnk, gf);

        const auto &[quot, mod] = GFPolynomial::divide(shifted_inf, generator, gf);

        const auto codeword = GFPolynomial::sum(shifted_inf, mod, gf);

        return codeword.bitmask_coeffs;
    }

    [[nodiscard]] vector<int> non_systematic_encode(const vector<int> &inf) const {
        if (inf.size() != k) {
            throw std::logic_error("Information vector is not of a code dimension length??");
        }
        // Non-systematic encoding is done by simply multiplying information vector by generating polynomial:
        const auto infp = GFPolynomial(inf);
        const auto result = GFPolynomial::multiply(infp, generator, gf);
        return result.bitmask_coeffs;
    }
};


class BMDecoder {
private:
    const int n, constr_min_dist;
    const ExtendedBinaryFiniteField &gf;
    const int b;
    GFPolynomial generator;
    vector<int> required_roots_deg;
public:
    BMDecoder(
            const int n,
            const int constrMinDist,
            const int b,
            const ExtendedBinaryFiniteField &gf
    ) :
            n(n),
            constr_min_dist(constrMinDist),
            gf(gf),
            b(b),
            generator({1}) {}

    // initialization is done as in encoder (it would have been better to merge them together into CoDec)
    void init() {
        for (int i = b; i <= b + constr_min_dist - 2; ++i) {
            required_roots_deg.push_back(i);
        }

        set<int> degrees_of_roots_of_generating_polynomial;

        if (et == RS) {
            degrees_of_roots_of_generating_polynomial =
                    set<int>(required_roots_deg.cbegin(), required_roots_deg.cend());
        } else if (et == BCH) {
            vector<set<int>> cyclotomic_classes(required_roots_deg.size());

            for (int i = 0; i < required_roots_deg.size(); ++i) {
                const auto root_deg = required_roots_deg[i];
                auto current_deg = root_deg;
                while (true) {
                    if (cyclotomic_classes[i].find(current_deg) != cyclotomic_classes[i].end()) {
                        break;
                    }
                    cyclotomic_classes[i].insert(current_deg);
                    current_deg *= 2; // Since p === 2, GF(2^m)
                    current_deg %= n;
                }
            }


            for (const auto &cc: cyclotomic_classes) {
                for (const auto deg: cc) {
                    degrees_of_roots_of_generating_polynomial.insert(deg);
                }
            }
        } else {
            throw std::runtime_error("Unknown encoder type");
        }

        for (const auto root_degree: degrees_of_roots_of_generating_polynomial) {
            const auto monome = GFPolynomial({gf.get_bitmask_by_degree(root_degree), 1});
            generator = GFPolynomial::multiply(generator, monome, gf);
        }
    }

    [[nodiscard]] vector<int> decode(const vector<int> &y) {
        if (y.size() != n) {
            throw std::logic_error("Received vector is not of a code size??");
        }

        const GFPolynomial infp = GFPolynomial(y);

        // First stage in decoding for B-M algorithm is computing syndrome polynomial:
        vector<int> syndrome_bitmasks;

        bool zero_syndrome = true;

        for (const auto root_deg: required_roots_deg) {
            const auto syndrome_bitmask =
                    GFPolynomial::evaluate_as_bitmask(
                            infp,
                            gf.get_bitmask_by_degree(root_deg),
                            gf);
            syndrome_bitmasks.push_back(syndrome_bitmask);
            if (syndrome_bitmask != 0) {
                zero_syndrome = false;
            }
        }

        // If syndrome is zero vector, it means that received vector is valid codeword, no need to do additional procedures.
        // (Of course, that could have happened even if it contained more errors than code can fix, but we compute that in error probability).
        if (zero_syndrome) {
            return y;
        }

        const GFPolynomial syndrome_polynomial = GFPolynomial(syndrome_bitmasks);

        // Construct error locator polynomial Л(z) = lambda(z) using B-M algorithm:
        GFPolynomial lambda({1}); // Locator polynomial
        {
            // Error locator polynomial computation is done inside this block in order not to pollute namespace of this function

            GFPolynomial prev_lambda({1}), prev_prev_extension_lambda({1});
            int prev_extension_iteration = 0, prev_extension_bias_bitmask = 1, current_length = 0;

            // For primitive binary BCH-codes we can yield optimization by skipping every other iteration:
            for (int i = 1; i <= syndrome_bitmasks.size(); i += 2) {
                // Current iteration's bias:
                int current_iter_bias_bitmask = 0;

                // Compute bias at current iteration:
                current_iter_bias_bitmask = syndrome_bitmasks[i - 1];
                for (int prev_lambda_power = 1; prev_lambda_power <= current_length; ++prev_lambda_power) {
                    const auto prev_lambda_jth_coeff_bitmask = prev_lambda[prev_lambda_power];
                    const auto syndrome_multiplier_bitmask = syndrome_bitmasks[i - prev_lambda_power - 1];
                    const auto new_term = gf.get_mult_bitmask(prev_lambda_jth_coeff_bitmask,
                                                              syndrome_multiplier_bitmask);
                    const auto new_bias_bitmask = gf.get_sum_bitmask(current_iter_bias_bitmask, new_term);
                    current_iter_bias_bitmask = new_bias_bitmask;
                }

                if (current_iter_bias_bitmask == 0) {
                    // Do not touch prev lambda
                    continue;
                } else {
                    // Recalculate current lambda approximation:
                    const int inverse_prev_extension_bitmask = gf.get_inv_bitmask(prev_extension_bias_bitmask);
                    const int fraction_bitmask =
                            gf.get_mult_bitmask(
                                    current_iter_bias_bitmask,
                                    inverse_prev_extension_bitmask
                            );
                    vector<int> second_term_multiplier_coeffs(1 + (i - prev_extension_iteration), 0);
                    second_term_multiplier_coeffs[i - prev_extension_iteration] = fraction_bitmask;
                    const GFPolynomial second_term =
                            GFPolynomial::multiply(
                                    GFPolynomial(second_term_multiplier_coeffs),
                                    prev_prev_extension_lambda,
                                    gf
                            );
                    const GFPolynomial new_lambda = GFPolynomial::sum(prev_lambda, second_term, gf);
                    const auto new_length = new_lambda.degree;

                    // Save values in case of register length increase:
                    if (new_length > current_length) {
                        current_length = new_length;
                        prev_prev_extension_lambda = prev_lambda;
                        prev_extension_bias_bitmask = current_iter_bias_bitmask;
                        prev_extension_iteration = i;
                    }

                    prev_lambda = new_lambda;
                }
            }

            // Assign value to the outer scope variable;
            lambda = prev_lambda;
        }

        // Find error locator polynomial roots using Chien's Procedure (a cool way to say `bruteforce` :) ):
        vector<int> locator_roots_degs;
        for (int i = 0; i < gf.power - 1; ++i) {
            const auto alpha_ith_power_bitmask = gf.get_bitmask_by_degree(i);
            const auto lambda_value = GFPolynomial::evaluate_as_bitmask(lambda, alpha_ith_power_bitmask, gf);
            if (lambda_value == 0) {
                locator_roots_degs.push_back(i);
            }
        }

        if (et == RS) {
            // In case of Reed-Solomon code, we also need to compute Г(z) polynomial from key equation of BCH decoding to get values of errors later:
            // Г(z) = Л(z)*S(z) mod z^(constructive_min_distance - 1)

            // Construct gamma polynomial:
            const auto pre_gp = GFPolynomial::multiply(lambda, syndrome_polynomial, gf);
            auto divisor_polynomial_coeffs = vector<int>(constr_min_dist);
            divisor_polynomial_coeffs[constr_min_dist - 1] = 1;
            const auto divisor_polynomial = GFPolynomial(divisor_polynomial_coeffs);
            const auto gamma_polynomial = GFPolynomial::divide(pre_gp, divisor_polynomial, gf).second;

            const auto d_lambda = GFPolynomial::derivate(lambda, gf);

            // For non-narrow-sense codes there should have been another simple procedure, but we do not need it in our task
            if (b != 1) {
                // To simplify and remove powering:
                throw std::runtime_error("Only codes with b=1 are now supported for decoding");
            }

            // Find values of errors as y = Г(z_root)/Л'(z_root) where Л'(z) is derivative polynomial of error locator polynomial
            vector<int> error_value_bitmasks(locator_roots_degs.size());

            for (int i = 0; i < locator_roots_degs.size(); ++i) {
                const auto inv_root_deg = locator_roots_degs[i];
                const auto inv_root_bitmask = gf.get_bitmask_by_degree(inv_root_deg);
                const auto numerator_bitmask = GFPolynomial::evaluate_as_bitmask(gamma_polynomial, inv_root_bitmask,
                                                                                 gf);
                const auto denominator_bitmask = GFPolynomial::evaluate_as_bitmask(d_lambda, inv_root_bitmask, gf);
                const auto inv_denominator_bitmask = gf.get_inv_bitmask(denominator_bitmask);
                const auto fraction_bitmask = gf.get_mult_bitmask(numerator_bitmask, inv_denominator_bitmask);
                error_value_bitmasks[i] = fraction_bitmask;
            }

            vector<int> fixed_codeword(y);

            // Fix codeword by adding error values at the error locations gained by roots of error locator polynomial:

            for (int i = 0; i < locator_roots_degs.size(); ++i) {
                const auto locator_root_deg = locator_roots_degs[i];
                const auto error_position = gf.get_inv_degree(locator_root_deg);
                const auto error_value_bitmask = error_value_bitmasks[i];
                const auto old_value_bitmask = y[error_position];
                const auto new_value_bitmask = gf.get_sum_bitmask(old_value_bitmask, error_value_bitmask);
                fixed_codeword[error_position] = new_value_bitmask;
            }

            return fixed_codeword;
        } else if (et == BCH) {
            // In case of BCH task, we can only have one possible error value (1):
            vector<int> fixed_codeword(y);
            // So, negate coordinates on error positions gained from roots of error locator polynomial:
            for (const auto inv_locator: locator_roots_degs) {
                const auto error_position = gf.get_inv_degree(inv_locator);
                fixed_codeword[error_position] ^= 0b1;
            }
            return fixed_codeword;
        } else {
            throw std::runtime_error("Unknown code type??");
        }
    }
};


int main() {
    // Standard techniques to speed up I/O:
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    // Fix seed for debug reproducibility:
    // gen.seed(768720956237485LL);

    // Open task files:
    FILE *in = fopen("input.txt", "r"), *out = fopen("output.txt", "w");

    if ((in == nullptr) || (out == nullptr)) {
        throw std::runtime_error("Cannot open task files.");
    }

    //int p = 2; // GF(p^n), here GF(2^n) in task
    int n, primitive_polynomial_bitmask, constr_mindist;

    fscanf(in, "%d %d %d", &n, &primitive_polynomial_bitmask, &constr_mindist);

    const int m = log2(n + 1);

    // Build Galois Field:
    ExtendedBinaryFiniteField gf = ExtendedBinaryFiniteField(m, primitive_polynomial_bitmask);

    // initialize encoder:
    auto encoder = CCEncoder(n, m, constr_mindist, 1 /* Так как БЧХ в узком смысле */, gf);
    encoder.init();

    // Get values that are parameterized by the code itself:

    // Code dimension k:
    const int code_dimension = encoder.get_dimension();
    fprintf(out, "%d\n", code_dimension);
    fflush(out);

    // Generator polynomial coefficients:
    const auto generator = encoder.get_generator();
    for (int i = 0; i <= generator.degree; ++i) {
        fprintf(out, "%d ", generator.bitmask_coeffs[i]);
    }
    fprintf(out, "\n");
    fflush(out);

    // Construct decoder:
    auto decoder = BMDecoder(n, constr_mindist, 1, gf);
    decoder.init();

    std::uniform_int_distribution<int> input_symbols_gen(0, n);
    std::uniform_int_distribution<int> err_symbols_gen(1, n);

    // Assertion on GF correct build:
    if (gf.power != 1 + n) {
        throw std::logic_error("Wrong field construction");
    }


    // Reserve a buffer for input command:
    char cmd[32] = {0x2A};

    while (true) {
        // Read command:
        int scanResult = fscanf(in, "%s", cmd);

        if (scanResult == EOF) {
            break;
        }

        // And act as designed:
        if (strcasecmp(cmd, "ENCODE") == 0) {
            // Read information vector:
            vector<int> inf_word(code_dimension);
            for (int i = 0; i < code_dimension; ++i) {
                fscanf(in, "%d", &inf_word[i]);
                if (inf_word[i] >= ((et == RS) ? gf.power : 2)) {
                    throw std::runtime_error("Incorrect input symbol");
                }
            }

            // And perform its systematic encoding:
            auto codeword = encoder.systematic_encode(inf_word);

            // Extend it with zeroes in case polynomial has degree less than code length n:
            while (codeword.size() < n) {
                codeword.push_back(0);
            }

            // Print out coefficients:
            for (const auto coeff: codeword) {
                // Correct output symbols assertion:
                if (coeff >= ((et == RS) ? (gf.power) : 2)) {
                    throw std::runtime_error("Incorrect output??");
                }
                fprintf(out, "%d ", coeff);
            }
            fprintf(out, "\n");
            fflush(out);
        } else if (strcasecmp(cmd, "DECODE") == 0) {
            // Read source encoded vector:
            vector<int> src_vector(n);
            for (int i = 0; i < n; ++i) {
                fscanf(in, "%d", &src_vector[i]);
                // Assert correct symbols:
                if (src_vector[i] >= ((et == RS) ? gf.power : 2)) {
                    throw std::runtime_error("Incorrect input symbol");
                }
            }
            // Decode using B-M algorithm:

            auto fixed_codeword = decoder.decode(src_vector);

            // Extend it with zeroes in case polynomial has degree less than code length n:
            while (fixed_codeword.size() < n) {
                fixed_codeword.push_back(0);
            }

            // Print out coefficients:
            for (const auto coeff: fixed_codeword) {
                fprintf(out, "%d ", coeff);
            }
            fprintf(out, "\n");
            fflush(out);
        } else if (strcasecmp(cmd, "SIMULATE") == 0) {
            float err_prob = 0.0;
            int max_iterations, max_errors;

            // Read simulation parameters:
            fscanf(in, "%f %d %d", &err_prob, &max_iterations, &max_errors);

            // And convert them to the integrals, because random int generation is noticeably faster than random float generation:
            const int err_prob_int = (int) (10000000.0f * err_prob);


            int iteration_count = 0, error_count = 0;

            // Iterate while stop conditions are not met:
            for (iteration_count = 0;
                 (iteration_count < max_iterations) && (error_count < max_errors);
                 ++iteration_count) {
                // Generate random input vector:
                vector<int> input(code_dimension);
                for (int i = 0; i < code_dimension; ++i) {
                    if (et == RS) {
                        input[i] = input_symbols_gen(gen);
                    } else if (et == BCH) {
                        input[i] = bitgen(gen);
                    }
                }

                // And encode it using non-systematic encoding (that's a way faster than systematic because it does not require polynomial division):
                vector<int> encoded_vector = encoder.non_systematic_encode(input);

                // Extend result with zeroes in case its polynomial has degree less than code length n:
                encoded_vector.resize(n, 0);

                // Add uniform noise to the encoded vector:
                vector<int> noisy_vector = vector<int>(encoded_vector.size());
                for (int i = 0; i < n; ++i) {
                    noisy_vector[i] = encoded_vector[i];
                    // Flip integral coin, not floating-point coin, it consumes less time:
                    const auto coin = coin_gen(gen); //prob_gen(gen);
                    //if (coin < err_prob){
                    if (coin < err_prob_int) {
                        // In case an error should be added to this position, generate random non-zero error value (or it could only be 1 in BCH case):
                        noisy_vector[i] =
                                gf.get_sum_bitmask(
                                        noisy_vector[i],
                                        (et == RS) ? (err_symbols_gen(gen)) : (1)
                                        );
                    }
                }

                // Ask decoder to decode noisy codeword:
                vector<int> restored_codeword = decoder.decode(noisy_vector);

                // Check all positions and recalculate statistics in case any bit is not restored correctly:
                for (int i = 0; i < n; ++i) {
                    if (restored_codeword[i] != encoded_vector[i]) {
                        ++error_count;
                        break;
                    }
                }
            }

            // Compute error probability (could be in doubles, because it is not computed frequently):
            double error_prob = ((double) error_count) / ((double) iteration_count);

            fprintf(out, "%.20lf\n", error_prob);
            fflush(out);
        }
    }

    fclose(in);
    fclose(out);
    return 0;
}